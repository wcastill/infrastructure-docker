#!/bin/bash

mysql_install_db -u mysql
service mysql start
mysql -e "UPDATE mysql.user SET Password = PASSWORD('Labo113') WHERE User = 'root'"
mysql -e "DROP USER ''@'localhost'"
mysql -e "DROP DATABASE test"
mysql -e "FLUSH PRIVILEGES"
mysql -e "CREATE DATABASE wordpress"
mysql -e "CREATE USER 'wcastillo'@'%' IDENTIFIED BY 'Labo113'"
mysql -e "CREATE USER 'wcastillo'@'localhost' IDENTIFIED BY 'Labo113'"
mysql -e "GRANT ALL PRIVILEGES ON * . * TO 'wcastillo'@'%';"
mysql -e "GRANT ALL PRIVILEGES ON * . * TO 'wcastillo'@'localhost';"
mysql -e "FLUSH PRIVILEGES;"
 echo "[mysqld]

user                    = mysql
pid-file                = /run/mysqld/mysqld.pid
socket                  = /run/mysqld/mysqld.sock
port                   = 3306
basedir                 = /usr
datadir                 = /var/lib/mysql
tmpdir                  = /tmp
lc-messages-dir         = /usr/share/mysql
bind-address            = 0.0.0.0

query_cache_size        = 16M

log_error = /var/log/mysql/error.log
server-id              = 20
log_bin                = /var/log/mysql/mysql-bin.log
expire_logs_days        = 10
binlog_do_db           = wordpress
character-set-server  = utf8mb4
collation-server      = utf8mb4_general_ci" > /etc/mysql/mariadb.conf.d/50-server.cnf
service mysql restart

Filedb2=`mysql -e "SHOW MASTER STATUS;" | awk '{print $1}' | cut -d 'e' -f2`
Positiondb2=`mysql -e "SHOW MASTER STATUS;" | awk '{print $2}' | cut -d 'n' -f2`

mysql -e "STOP SLAVE;"
mysql -e "CHANGE MASTER TO MASTER_HOST = '172.40.0.11',MASTER_USER = 'wcastillo', MASTER_PASSWORD = 'Labo113', MASTER_LOG_FILE = 'mysql-bin.000001', MASTER_LOG_POS = $Positiondb2;"
mysql -e "START SLAVE;"

sleep 9999999
