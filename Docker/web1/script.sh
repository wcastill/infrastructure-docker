#!/usr/bin/php -qC

function get_args()
{
        $args = array();
        for ($i=1; $i<count($_SERVER['argv']); $i++)
        {
                $arg = $_SERVER['argv'][$i];
                if ($arg{0} == '-' && $arg{1} != '-')
                {
                        for ($j=1; $j < strlen($arg); $j++)
                        {
                                $key = $arg{$j};
                                $value = $_SERVER['argv'][$i+1]{0} != '-' ? preg_replace(array('/^["\']/', '/["\']$/'), '', $_SERVER['argv'][++$i]) : true;
                                $args[$key] = $value;
                        }
                }
                else
                        $args[] = $arg;
        }

        return $args;
}

// read commandline arguments
$opt = get_args();

require "install.php";
