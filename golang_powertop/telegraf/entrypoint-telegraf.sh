#!/bin/bash

echo "Export env"
echo ""

export PATH=$PATH:/usr/local/go/bin
export PATH=$PATH:$(go env GOPATH)/bin
export GOPATH=$(go env GOPATH)

git clone https://github.com/influxdata/telegraf.git
cd telegraf/
git checkout -b powertop

export IP_INFLUXDB="$(cat /powertop/ip_influxdb)"
export URL_INFLUXDB="http://$IP_INFLUXDB:8086"

echo ""
echo "Create telegraf conf"
echo ""
cat <<EOF >powertop.conf
[agent]
    interval = "60s"
[[outputs.influxdb]]
    urls = [ "$URL_INFLUXDB" ]
    database = "telegraf_powertop"
    skip_database_creation = false
[[inputs.powertop]]
EOF

sed -i '$d' plugins/inputs/all/all.go
echo -e "        _ \"github.com/influxdata/telegraf/plugins/inputs/powertop\"\n)" >>plugins/inputs/all/all.go

echo "Make Telegraf"
echo $PWD
mkdir plugins/inputs/powertop
cp /powertop/powertop.go plugins/inputs/powertop/powertop.go
make telegraf

echo "Start Telegraf"
./telegraf --config powertop.conf --debug 
