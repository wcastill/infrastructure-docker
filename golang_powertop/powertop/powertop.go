package powertop

import (
	"encoding/csv"
	"log"
	"time"
	"os/exec"
	"strings"
	"github.com/influxdata/telegraf"
	"github.com/influxdata/telegraf/plugins/inputs"
)

type Powertop struct {
	Usage		string `csv:"Usage"`
	Wakeups		string `csv:"Wakeups"`
	GFX_wakeups	string `csv:"GFX_wakeups"`
	Category        string `csv:"Category"`
	Descript	string `csv:"Description"`
	Power		string `csv:"Power"`
}

func (p *Powertop) Description() string {
	return "Gather Powertop infos"
}

func (p *Powertop) SampleConfig() string {
	return ""
}

func (p *Powertop) Gather(acc telegraf.Accumulator) error {

	NewCSV, err := exec.Command("sed", "-n", "/Usage;Wakeups/,/^$/p", "/powertop/powertop.csv").Output()
	if err != nil {
		log.Fatal(err)
	}
	csv_read := csv.NewReader(strings.NewReader(string(NewCSV)))
	csv_read.Comma = ';'
	csv_read.FieldsPerRecord = -1
	datas, err := csv_read.ReadAll()
	if err != nil {
		log.Fatal(err)
	}
	// split without header
	for _, line := range datas[1:] {

		fields := map[string]interface{}{
			"Usage":	line[0],
			"Wakeups":	line[1],
			"GFX_wakeups":	line[4],
			"Category":	line[5],
			"Power":	line[7],
		}

		tags := map[string]string{
                        "Description":  line[6],
                }

		now := time.Now()

		acc.AddFields("powertop", fields, tags, now)
	}
	return nil
}

func init() {
	inputs.Add("powertop", func() telegraf.Input {
		return &Powertop{}
	})
}
