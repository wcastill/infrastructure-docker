#!/bin/bash

RED="\033[01;31m"
YELLOW="\033[01;33m"
CYAN="\033[01;36m"
WHITE="\033[01;37m"
BLUE="\033[01;34m"

export PATH_GOLANG_PROJECT="$PWD"

(crontab -l; echo "PATH_GOLANG_PROJECT="$PWD"") | sort - | uniq - | crontab
(crontab -l; echo "SHELL=/bin/bash") | sort - | uniq - | crontab
(crontab -l; echo "* * * * *              sh $PATH_GOLANG_PROJECT/crea_csv_powertop.sh") | sort - | uniq - | crontab
(crontab -l; echo "* * * * * (sleep 30 ; sh $PATH_GOLANG_PROJECT/crea_csv_powertop.sh)") | sort - | uniq - | crontab

docker-compose up -d
docker exec influxdb influx -username 'admin' -password 'admin' -execute 'Create database telegraf_powertop'

export IP_GRAFANA=$(docker exec grafana hostname -i)
export IP_INFLUXDB=$(docker exec influxdb hostname -i)

echo ""
echo -e "${RED}******************************************************************************"
echo -e "${RED}**                                                                          **"
echo -e "${RED}**          ${WHITE}Installation d'InfluxDB + Telegraf + Grafana terminé            ${RED}**"
echo -e "${RED}**                                                                          **"
echo -e "${RED}******************************************************************************"
echo ""
echo -e "${WHITE}** Hello ${CYAN}${USER} ${WHITE}! Bienvenue dans notre projet powertop"
echo -e "${WHITE}** Pour vérifier les données connecte toi à Grafana"
echo ""
echo -e "${YELLOW}*****************************  Informations connexion Grafana:"
echo -e "${YELLOW}**                         **  URL: ${RED}http://$IP_GRAFANA:3000"
echo -e "${YELLOW}**        GRAFANA          **  USER: ${RED}admin"
echo -e "${YELLOW}**                         **  PASSWORD: ${RED}admin"
echo -e "${YELLOW}*****************************  "
echo ""
echo -e "${BLUE}*****************************  Informations connexion InfluxDB:"
echo -e "${BLUE}**                         **  URL: ${RED}http://$IP_INFLUXDB:8086"
echo -e "${BLUE}**        INFLUXDB         **  DATABASE: ${RED}telegraf_powertop"
echo -e "${BLUE}**                         **  USER: ${RED}admin"
echo -e "${BLUE}*****************************  PASSWORD: ${RED}admin"
echo ""
